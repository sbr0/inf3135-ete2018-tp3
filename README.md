# Travail pratique 3

## Description

Traitement et affichage de données géographiques issues du projet [countries](https://github.com/mledoze/countries).

Ce projet à été effectué dans le cadre du cours `INF3135 - Construction et maintenance de logiciels` à l'`UQAM` durant la session d'été 2018 pour le professeur `Alexandre Blondin-Massé`

## Auteur

- Simon Brown (BROS06109406)

## Fonctionnement

### But

Ce Programme permet d'afficher des information sur des pays sous différents
formats, dont le format `dot` compatible avec le  logiciel graphviz, ou encore
le format `png` qui sort directement une image représentant les données.

### Installation

Le programme peut être installé simplement avec les commandes suivantes:
```sh

$ git clone https://gitlab.com/sbr0/inf3135-ete2018-tp3/

$ make
```

Voir la section [dépendances](#dépendances) pour voir les dépendances nécessaires.

### Utilisation
```sh
$ ./bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]
 [--show-languages] [--show-capital] [--show-borders] [--show-flag]
 [--country COUNTRY] [--region REGION]

Displays information about countries.

Optional arguments:
  --help                     Show this help message and exit.
  --output-format FORMAT     Selects the ouput format (either "text", "dot" or "png").
                             The "dot" format is the one recognized by Graphviz.
                             The default format is "text".
  --output-filename FILENAME The name of the output filename. This argument is
                             mandatory for the "png" format. For the "text" and "dot"
                             format, the result is printed on stdout if no output
                             filename is given.
  --show-languages           The official languages of each country are displayed.
  --show-capital             The capital of each country is displayed.
  --show-borders             The borders of each country are displayed.
  --show-flag                The flag of each country is displayed
                             (only for "dot" and "png" format).
  --country COUNTRY          The country code (e.g. "can", "usa") to be displayed.
  --region REGION            The region of the countries to be displayed.
                             The supported regions are "africa", "americas",
                             "asia", "europe" and "oceania".

```

### Exemple

Par exemple la commande
```sh
$ ./bin/tp3 --show-languages --show-capital --show-borders --country can
```
affichera le texte suivant sur la sortie standard `stdout`:
```
Name: Canada
Code: CAN
Capital: Ottawa
Languages: English, French
Borders: USA
```

Les commandes 
```sh
$ ./bin/tp3 --show-capital --show-flag --output-format dot --country fra | neato -Goverlap=false -Tpng -o france.png
```
et
```sh
$ ./bin/tp3 --show-capital --show-flag --output-format png --country fra --output-filename france.png
```
sont équivalentes et produisent toutes les deux l'image suivante:
![](images/france.png)

## Plateformes supportées

* `Arch linux (64-bit)` est la seule plateforme qui a été testée
* `Ubuntu` et `OS X` devrait être compatible aussi.

## Dépendances

* [Jansson](http://www.digip.org/jansson/)
* [Graphviz](https://graphviz.org/)
* [Bats](https://github.com/sstephenson/bats)

## Références
* Alexandre Blondin-Massé:
  * inf3135-ete2018-tp2
  * inf3135-ete2018-tp3-enonce
  * Notes de cours
* [Stack overflow](https://stackoverflow.com/)

## Division des tâches

Le travail pratique à été réalisé seul.

## État du projet

- [X] Gestion des options `--show-languages`, `--show-capital`, `show-borders`;
- [X] Gestion de l’option `--show-flag`;
- [X] Gestion des options `--country` et `--region`;
- [X] Format de sortie `text`;
- [X] Format de sortie `dot`;
- [X] Format de sortie `png`;
- [X] Compilation simple (make);
- [ ] Couverture de tests (make test) et intégration continue (.gitlab-ci.yml);
- [X] Documentation du fonctionnement (README).

