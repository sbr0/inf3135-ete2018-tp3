SRC_DIR = src
BIN_DIR = bin
BATS_FILE = test.bats
EXEC = tp3
 
.PHONY: exec bindir clean source test
 
exec: source bindir
	cp $(SRC_DIR)/$(EXEC) $(BIN_DIR)
 
bindir:
	mkdir -p $(BIN_DIR)
 
clean:
	make clean -C $(SRC_DIR)
	rm -rf $(BIN_DIR)

source:
	make -C $(SRC_DIR)
	
test: exec testbats

testbats:
	bats $(BATS_FILE)
