#!/usr/bin/env bats

# Temporary directory for tests

mkdir -p "./tmp"
BATS_TMPDIR="./tmp"

@test "Default program working" {
  run bin/tp3
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Name: Aruba" ]
}

@test "Wrong country code" {
  run bin/tp3 --country fdsa 
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "error: No country matches criteria" ]
}

@test "Wrong region" {
  run bin/tp3 --region afgaegr
  [ "$status" -eq 2 ]
  [ "${lines[0]}" = "error: Invalid region" ]
}

@test "Wrong format" {
  run bin/tp3 --output-format gs
  [ "$status" -eq 1 ]
  [ "${lines[0]}" = "error: Invalid output format" ]
}
