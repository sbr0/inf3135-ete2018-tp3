/**
 * Responsible for parsing the main arguments.
 *
 * @author Simon Brown
 * @author Alexandre Blondin Masse
 */
#ifndef PARSE_ARGS_H
#define PARSE_ARGS_H

#include <stdbool.h>

#define USAGE "\
Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]\n\
 [--show-languages] [--show-capital] [--show-borders] [--show-flag]\n\
 [--country COUNTRY] [--region REGION]\n\
\n\
Displays information about countries.\n\
\n\
Optional arguments:\n\
  --help                     Show this help message and exit.\n\
  --output-format FORMAT     Selects the ouput format (either \"text\", \"dot\" or \"png\").\n\
                             The \"dot\" format is the one recognized by Graphviz.\n\
                             The default format is \"text\".\n\
  --output-filename FILENAME The name of the output filename. This argument is\n\
                             mandatory for the \"png\" format. For the \"text\" and \"dot\"\n\
                             format, the result is printed on stdout if no output\n\
                             filename is given.\n\
  --show-languages           The official languages of each country are displayed.\n\
  --show-capital             The capital of each country is displayed.\n\
  --show-borders             The borders of each country are displayed.\n\
  --show-flag                The flag of each country is displayed\n\
                             (only for \"dot\" and \"png\" format).\n\
  --country COUNTRY          The country code (e.g. \"can\", \"usa\") to be displayed.\n\
  --region REGION            The region of the countries to be displayed.\n\
                             The supported regions are \"africa\", \"americas\",\n\
                             \"asia\", \"europe\" and \"oceania\".\n\
"

/** 
 * Program status.
 */
enum Status {
    TP3_OK,                 /**< Everything is ok so far */
    TP3_WRONG_OUT_FORMAT,   /**< Invalid output format */
    TP3_WRONG_REGION,       /**< Invalid region argument */
    TP3_TOO_MANY_ARGUMENTS, /**< Too many arguments */
    TP3_INCONSISTENT_ARGS,  /**< Some arguments are inconsistent */
    TP3_FILE_ERROR,         /**< File related error */
    TP3_BAD_OPTION          /**< Bad option */
};

/**
 * Parsed argument struct.
 */
struct Arguments {
    char *country;          /**< The county to show */
    char *region;           /**< The region to show */
    char *output_format;    /**< The region to show */
    char *output_filename;  /**< The region to show */
    bool show_languages;    /**< Show spoken languages? */
    bool show_capital;      /**< Show capital? */
    bool show_borders;      /**< Show countries with a shared border? */
    bool show_flag;         /**< Show the flag? */
    bool exit_on_ret;       /**< Exit immediatly, successfully */
    enum Status status;     /**< The status of the parsing */
};

/**
 * Prints how to use the program.
 */
void print_usage();

/**
 * Returns the struct of parsed arguments.
 *
 * @param argc  Number of arguments
 * @param argv  Array of arguments
 * @return      Struct of parsed arguments
 */
struct Arguments *parse_arguments(int argc, char **argv);

/**
 * Frees the memory allocated for the arguments.
 *
 * @param arguments  Struct of arguments to free
 */
void free_arguments(struct Arguments *arguments);

#endif
