/**
 * Implements parse_args.h with the getopt library
 *
 * @author Simon Brown
 * @author Alexandre Blondin Masse
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include "parse_args.h"

/**
 * Verifies if region is valid.
 *
 * Compares against known list of regions ("africa", "americas", "asia",
 * "europe" or "oceania").
 *
 * @param region    region input to verify
 * @return          true if valid, false otherwise
 */
bool _is_valid_region(char *region){
    if (strcmp(region, "africa") != 0 &&
        strcmp(region, "americas") != 0 &&
        strcmp(region, "asia") != 0 &&
        strcmp(region, "europe") != 0 &&
        strcmp(region, "oceania") != 0
       ) {
        return false;
    }
    return true;
}
/**
 * Verifies if output-format is valid
 *
 * Compares against supported formats ("text", "dot" and "png").
 * 
 * @param format    format to verify
 * @return          true if valid, false othewise
 */
bool _is_output_format_valid(char *format){
    if (strcmp(format, "text") != 0 &&
        strcmp(format, "dot") != 0 &&
        strcmp(format, "png") != 0
       ) {
        return false;
    }
    return true;
}

void print_usage() {
    fprintf(stderr, USAGE);
}

struct Arguments *parse_arguments(int argc, char *argv[]) {
    struct Arguments *arguments = malloc(sizeof(struct Arguments));

    bool show_help = false;

    // Defaults
    arguments->status = TP3_OK;
    arguments->country = NULL;
    arguments->region = NULL;
    arguments->output_format = "text";
    arguments->output_filename = NULL;
    arguments->show_languages = false;
    arguments->show_capital = false;
    arguments->show_borders = false;
    arguments->show_flag = false;
    arguments->exit_on_ret = false;

    // Resets index
    optind = 0;
    struct option long_opts[] = {
        // Set flag
        {"help",            no_argument,       0, 'h'},
        {"show-languages",  no_argument,       0, 'l'},
        {"show-capital",    no_argument,       0, 'a'},
        {"show-borders",    no_argument,       0, 'b'},
        {"show-flag",       no_argument,       0, 'f'},
        // Don't set flag
        {"output-format",   required_argument, 0, 't'},
        {"output-filename", required_argument, 0, 'o'},
        {"country",         required_argument, 0, 'c'},
        {"region",          required_argument, 0, 'r'},
        {0, 0, 0, 0}
    };

    // Parse options
    while (true) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "hlabf:t:o:c:r:", long_opts, &option_index);
        if (c == -1) break;
        switch (c) {
            case 'h': show_help = true;
                      arguments->exit_on_ret = true;
                      break;
            case 'l': arguments->show_languages = true;
                      break;
            case 'a': arguments->show_capital = true;
                      break;
            case 'b': arguments->show_borders = true;
                      break;
            case 'f': arguments->show_flag = true;
                      break;
            case 't': arguments->output_format = optarg;
                      break;
            case 'o': arguments->output_filename = optarg;
                      break;
            case 'c': arguments->country = optarg;
                      break;
            case 'r': arguments->region = optarg;
                      break;
            case '?': if (arguments->status == TP3_OK) {
                          arguments->status = TP3_BAD_OPTION;
                      } 
                      break;
        }
    }

    if (optind < argc) {
        fprintf(stderr, "error: Too many arguments\n");
        arguments->status = TP3_TOO_MANY_ARGUMENTS;
        show_help = true;
    }
    if (arguments->country && arguments->region) {
        fprintf(stderr, "error: --country and --region option cannot be "
                        "used simultaniously.\n");
        arguments->status = TP3_INCONSISTENT_ARGS;
        show_help = true;
    }
    if (arguments->status == TP3_BAD_OPTION) {
        fprintf(stderr, "error: Unrecognised option\n");
        show_help = true;        
    }
    if (arguments->region && !_is_valid_region(arguments->region)) {
        fprintf(stderr, "error: Invalid region\n");
        arguments->status = TP3_WRONG_REGION;
        show_help = true;
    }
    if (!_is_output_format_valid(arguments->output_format)) {
        fprintf(stderr, "error: Invalid output format\n");
        arguments->status = TP3_WRONG_OUT_FORMAT;
        show_help = true;
    }
    if (strcmp(arguments->output_format, "png") == 0 && !arguments->output_filename) {
        fprintf(stderr, "error: Output filename must be provided for \"png\" format\n");
        arguments->status = TP3_INCONSISTENT_ARGS;
        show_help = true;
    }
    if (arguments->show_flag && (strcmp(arguments->output_format, "text") == 0)) {
        fprintf(stderr, "error: --show-flag option can only be used for \"png\""
                        " or \"dot\" output formats\n");
        arguments->status = TP3_INCONSISTENT_ARGS;
        show_help = true;
    }
    if (show_help) {
        print_usage();
    }

    return arguments;
}

void free_arguments(struct Arguments *arguments) {
    free(arguments);
}
