/**
 * Responsible for parsing json.
 *
 * @author Simon Brown
 */
#ifndef PARSE_JSON_H
#define PARSE_JSON_H

#include "parse_args.h"
#include <jansson.h>
#include <stdio.h>

#define MAX_STRING_LENGTH 300
#define MAX_COUNTRIES 275
#define FCT_SUCCESS 0
#define FCT_ERROR -1
#define LIST_SEPERATOR ","

/** 
 * Country status.
 */
enum Country_status {
    COUNTRY_OK,                 /**< Everything is ok so far */
    COUNTRY_BAD_NAME,           /**< Invalid country name */
    COUNTRY_BAD_CODE,           /**< Invalid country code */
    COUNTRY_BAD_CAPITAL,        /**< Invalid country capital */
    COUNTRY_BAD_LANGUAGES,      /**< Invalid country languages */
    COUNTRY_BAD_BORDERS,        /**< Invalid country borders */
    COUNTRY_BAD_REGION          /**< Invalid country region */
};

/**
 * Country struct.
 */
struct Country {
    int number;                 /**< The country's position in file */
    char *name;                 /**< The country's name */
    char *code;                 /**< The country's code */
    char *capital;              /**< The country's capital */
    char *languages;            /**< The country's languages */
    char *borders;              /**< The country's borders */
    const char *region;         /**< The country's region */
    enum Country_status status; /**< The status of the parsing */
};

/**
 * Read file containing json country data.
 *
 * The returned string must be freed wiht free_json_countries().
 *
 * @return  string containg raw json data
 */
char * read_json_countries(char * json_abs_path);

/**
 * Free the memory allocated to store json_countries string.
 *
 * @param json_countries    pointer to json_countries string
 */
void free_json_countries(char *json_countries);

/**
 * Free array of Country structs.
 *
 * @param countries     pointer to array of countries to be freed
 */
void free_countries(struct Country **countries);

/**
 * Parse raw json data to an array of countries according to specified arguments.
 *
 * @param countries         pre-allocated array of Country structs
 * @param json_countries    string containg raw json country data
 * @param arguments         struct of arguments used for filtering
 * @return                  number of countries processed or -1 if error
 */
int parse_json(struct Country ** countries, char * json_countries, struct Arguments * arguments);

/**
 * Print the contents of an array of countries in a simple text format
 * according to arguments properties to a specified file.
 *
 * @param countries     array of Country structs to be displayed
 * @param arguments     struct of arguments used to select data to print
 * @param f             open file descriptor to print to
 */
void print_text_countries(struct Country ** countries, struct Arguments * arguments, FILE *f);

#endif
