#include <stdio.h>
#include <string.h>
#include <libgen.h>
#include "parse_args.h"
#include "parse_json.h"
#include "graphics.h"

#define MAX_PATH_LEN 100
#define JSON_REL_PATH "/../countries/countries.json"

int main(int argc, char **argv) {
    char *json_abs_path = malloc(MAX_PATH_LEN);
    struct Country *countries[MAX_COUNTRIES];
    struct Arguments *arguments;
    FILE *out_fd = stdout;
    char *json_countries;
    int ret = 0;
    
    arguments = parse_arguments(argc, argv);
    if (arguments->status != TP3_OK)
        ret =  arguments->status;
 
    strcpy(json_abs_path, dirname(argv[0]));
    strcat(json_abs_path, JSON_REL_PATH);

    if (ret == 0 && !arguments->exit_on_ret) {
        json_countries = read_json_countries(json_abs_path);
        if (!json_countries) {
            fprintf(stderr, "error: reading json file failed\n");
            ret = TP3_FILE_ERROR;
        }
        if (ret == 0) { 
            int a = parse_json(countries, json_countries, arguments);
            if (a == -1) {
                fprintf(stderr, "error: parsing json data failed\n");
                ret = TP3_FILE_ERROR;
            } else if (a == 0) {
                fprintf(stderr, "error: No country matches criteria\n");
            }
        }
        if (ret == 0) {
            if (strcmp(arguments->output_format, "text") == 0 || strcmp(arguments->output_format, "dot" ) == 0) {
                if (arguments->output_filename) {
                    out_fd = fopen (arguments->output_filename, "w");
                    if (!out_fd){
                        fprintf(stderr, "error: output-file %s could not be opened\n", arguments->output_filename);
                        ret = TP3_FILE_ERROR;
                    }
                }
            }
        }
        
        if (ret == 0) {
            if (strcmp(arguments->output_format, "text") == 0) {
                print_text_countries(countries, arguments, out_fd);
            } else if (strcmp(arguments->output_format, "dot") == 0) {
                print_dot_countries(countries, arguments, out_fd);
            } else if (strcmp(arguments->output_format, "png") == 0) {
                if (gen_png_countries(countries, arguments) != 0)
                    ret = TP3_FILE_ERROR;
            }
        }
        fclose(out_fd);
        free_json_countries(json_countries);
        free_countries(countries);
    }
    free_arguments(arguments);
    free(json_abs_path);
    return ret;
}
