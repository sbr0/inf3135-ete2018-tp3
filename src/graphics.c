#define _DEFAULT_SOURCE

#include "graphics.h"
#include <string.h>
#include <ctype.h>
#include <stdio.h>

/**
 * Retrun a lowercase copy of a mixed case string.
 *
 * Must be freed.
 *
 * @param mixed_case    string to be copied
 * @return              lower case copy of mixed_case
 */
char * _to_lower(char *mixed_case) {
    char* lower = calloc((strlen(mixed_case) + 1), sizeof(char));
    for (int i = 0; mixed_case[i]; i++) {
        lower[i] = tolower(mixed_case[i]);
    }
    return lower;
}

/**
 * Ouptuts dot format links from country to every other country in countries
 * before it with whom it shares a border.
 *
 * @param country     country who's borders will be printed
 * @param countries   list of countries to use as reference
 * @param f           file descriptor to print to
 */
void _write_country_borders(struct Country *country, struct Country **countries, FILE *f) {
    char *borders = country->borders;
    char *border = malloc(MAX_CODE_LEN);
    char *lower_country_code = _to_lower(country->code);
    do {
        size_t field_len = strcspn(borders, LIST_SEPERATOR);
        strncpy(border, borders, field_len);
        border[field_len] = '\0';
        char * lower_border= _to_lower(border);

        for (int i = 0; countries[i]->number < country->number; i++) {
            if (strcmp(countries[i]->code, border) == 0) {
                fprintf(f, "    %s -- %s;\n", lower_country_code, lower_border);
                break;
            }
        }
        free(lower_border);
        borders += field_len;
    } while (*borders++);
    free(border);
    free(lower_country_code);
}

void print_dot_countries(struct Country **countries, struct Arguments *args, FILE *f){
    fprintf(f, "graph {\n");
    size_t i = 0;
    while ((struct Country*)countries[i]) {
        char * lower_country_code = _to_lower(countries[i]->code);
        fprintf(f, "    %s [\n", lower_country_code);
        fprintf(f, "        shape = none,\n");
        fprintf(f, "        label = <<table border=\"0\" cellspacing=\"0\">\n");
        if (args->show_flag) {
            fprintf(f, "        <tr><td align=\"center\" border=\"1\" fixedsize=\"true\" width=\"200\" height=\"100\">");
            fprintf(f, "<img src=\"%s/%s.png\" scale=\"true\"/>", PNG_FLAG_DIR, lower_country_code);
            fprintf(f, "</td></tr>\n");
        }
        fprintf(f, "        <tr><td align=\"left\" border=\"1\"><b>Name</b>: %s</td></tr>\n", countries[i]->name);
        fprintf(f, "        <tr><td align=\"left\" border=\"1\"><b>Code</b>: %s</td></tr>\n", countries[i]->code);
        if (args->show_capital) {
            fprintf(f, "            <tr><td align=\"left\" border=\"1\"><b>Capital</b>: %s</td></tr>\n", countries[i]->capital);
        }
        if (args->show_languages) {
            fprintf(f, "            <tr><td align=\"left\" border=\"1\"><b>Languages</b>: %s</td></tr>\n", countries[i]->languages);
        }
        if (args->show_borders) {
            fprintf(f, "            <tr><td align=\"left\" border=\"1\"><b>Borders</b>: %s</td></tr>\n", countries[i]->borders);
        }
        fprintf(f, "        </table>>\n");
        fprintf(f, "    ];\n");
        
        free(lower_country_code);
        i++;
    }
    i = 0;
    while (countries[i]) {
        _write_country_borders(countries[i], countries, f);
        i++;
    }
    fprintf(f, "}\n");
}

int gen_png_countries(struct Country **countries, struct Arguments *arguments) {
    char cmd[MAX_CMD_LEN];
    strcpy(cmd, "neato -Goverlap=false -Tpng -o ");
    if (strlen(cmd) + strlen(arguments->output_filename) > MAX_CMD_LEN) {
        fprintf(stderr, "error: output-filename too long\n");
        return -1;
    }
    strcat(cmd, arguments->output_filename);
    FILE *fp = popen(cmd, "w");
    if (!fp) {
        fprintf(stderr, "error: neato sub-process launch failed\n");
        return -1;
    }

    print_dot_countries(countries, arguments, fp);

    if (pclose(fp) != 0) {
        fprintf(stderr, "error: closing sub-process failed\n");
    }
    return 0;
}
