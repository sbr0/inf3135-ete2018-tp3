#include "parse_json.h"
#include "parse_args.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/**
 * Compare two strings ignoring case
 *
 * @param a     first string to compare
 * @param b     second string to compare
 * @return      0 if equal, or integer difference between a and b
 */
int _str_cmp(char const *a, char const *b) {
    for (;; a++, b++) {
        int d = tolower(*a) - tolower(*b);
        if (d != 0 || !*a)
            return d;
    }
}

/**
 * Extract country's name from raw json data
 *
 * @param country   country structure to store result in
 * @param data      raw json single country data
 */
void _get_country_name(struct Country *country, json_t *data) {
    json_t *name = json_object_get(data, "name");
    if (name) {
        name = json_object_get(name, "official");
    }
    if (!json_is_string(name)) {
        fprintf(stderr, "error: country %d: Name is not a string\n", country->number + 1);
        country->status = COUNTRY_BAD_NAME;
        return;
    }
    country->name = malloc((strlen(json_string_value(name)) + 1) * sizeof(char));
    strcpy(country->name, json_string_value(name));
}

/**
 * Extract country's code from raw json data
 *
 * @param country   country structure to store result in
 * @param data      raw json single country data
 */
void _get_country_code(struct Country *country, json_t *data) {
    json_t *code = json_object_get(data, "cca3");
    if (!json_is_string(code)) {
        fprintf(stderr, "error: country %d: Code is not a string\n", country->number + 1);
        country->status = COUNTRY_BAD_CODE;
        return;
    }
    country->code = malloc((strlen(json_string_value(code)) + 1) * sizeof(char));
    strcpy(country->code, json_string_value(code));
}

/**
 * Extract country's capital from raw json data
 *
 * @param country   country structure to store result in
 * @param data      raw json single country data
 */
void _get_country_capital(struct Country *country, json_t *data) {
    char *result = malloc(MAX_STRING_LENGTH * sizeof(char));
    json_t *capitals = json_object_get(data, "capital");
    result[0] = '\0';
    if (!json_is_array(capitals)) {
        fprintf(stderr, "error: country %d: Capital is not an array\n", country->number + 1);
        country->status = COUNTRY_BAD_CAPITAL;
        return;
    }
    for (int i = 0; i < (int)json_array_size(capitals); i++) {
        json_t *capital = json_array_get(capitals, i);
        if (!json_is_string(capital)) {
            fprintf(stderr, "error: country %d: Capital is not a string\n", country->number + 1);
            country->status = COUNTRY_BAD_CAPITAL;
            return;
        }
        if (i > 0 && (strlen(result) + strlen(LIST_SEPERATOR) + 1 < MAX_STRING_LENGTH)) {
            strcat(result, LIST_SEPERATOR);
        }
        if (strlen(result) + strlen(json_string_value(capital)) + 1 < MAX_STRING_LENGTH) {
            strcat(result, json_string_value(capital));
        } else {
            fprintf(stderr, "error: country %d: Capital string too long\n", country->number +1);
            country->status = COUNTRY_BAD_CAPITAL;
            return;
        }
    }
    country->capital = result;
}

/**
 * Extract country's comma seperated borders from raw json data
 *
 * @param country   country structure to store result in
 * @param data      raw json single country data
 */
void _get_country_borders(struct Country *country, json_t *data) {
    char *result = malloc(MAX_STRING_LENGTH * sizeof(char));
    json_t *borders = json_object_get(data, "borders");
    result[0] = '\0';
    if (!json_is_array(borders)) {
        fprintf(stderr, "error: country %d: Borders is not an array\n", country->number + 1);
        country->status = COUNTRY_BAD_BORDERS;
        return;
    }
    for (int i = 0; i < (int)json_array_size(borders); i++) {
        json_t *border = json_array_get(borders, i);
        if (!json_is_string(border)) {
            fprintf(stderr, "error: country %d: Border is not a string\n", country->number + 1);
            country->status = COUNTRY_BAD_BORDERS;
            return;
        }
        if (i > 0 && (strlen(result) + strlen(LIST_SEPERATOR) + 1 < MAX_STRING_LENGTH)) {
            strcat(result, LIST_SEPERATOR);
        }
        if (strlen(result) + strlen(json_string_value(border)) + 1 < MAX_STRING_LENGTH) {
            strcat(result, json_string_value(border));
        } else {
            fprintf(stderr, "error: country %d: Border string too long\n", country->number +1);
            country->status = COUNTRY_BAD_BORDERS;
            return;
        }
    }
    country->borders = result;
}

/**
 * Extract country's comma seperated languages from raw json data
 *
 * @param country   country structure to store result in
 * @param data      raw json single country data
 */
void _get_country_languages(struct Country *country, json_t *data) {
    char *result = malloc(MAX_STRING_LENGTH * sizeof(char));
    json_t *languages = json_object_get(data, "languages");
    result[0] = '\0';
    if (!json_is_object(languages)) {
        fprintf(stderr, "error: country %d: languages is not an object\n", country->number + 1);
        country->status = COUNTRY_BAD_LANGUAGES;
        return;
    }

    const char *key;
    json_t *value;

    json_object_foreach(languages, key, value) {
        if (!json_is_string(value)) {
            fprintf(stderr, "error: country %d: language is not a string\n", country->number + 1);
            country->status = COUNTRY_BAD_LANGUAGES;
            return;
        }
        if (strlen(result) > 0 && (strlen(result) + strlen(LIST_SEPERATOR) + 1 < MAX_STRING_LENGTH)) {
            strcat(result, LIST_SEPERATOR);
        }
        if (strlen(result) + strlen(json_string_value(value)) + 1 < MAX_STRING_LENGTH) {
            strcat(result, json_string_value(value));
        } else {
            fprintf(stderr, "error: country %d: languages string too long\n", country->number +1);
            country->status = COUNTRY_BAD_LANGUAGES;
            return;
        }
    }
    country->languages = result;
}

/**
 * Extract country's region from raw json data
 *
 * @param country   country structure to store result in
 * @param data      raw json single country data
 */
void _get_country_region(struct Country *country, json_t *data) {
    json_t *region = json_object_get(data, "region");
    if (!json_is_string(region)) {
        fprintf(stderr, "error: country %d: Region is not a string\n", country->number + 1);
        country->status = COUNTRY_BAD_REGION;
        return;
    }
    country->region = json_string_value(region);
}

/**
 * Create full Country struct from raw single country json data.
 *
 * Must be freed with _free_country
 *
 * @param country_json      raw json single country data
 * @param country_number    position of country in raw list of countries
 * @return                  fully populated Country struct 
 */
struct Country *_get_country(json_t * country_json, int country_number){
    struct Country *country = malloc(sizeof(struct Country));
    country->status = COUNTRY_OK;
    country->number = country_number;

    _get_country_name(country, country_json);

    if (country->status == COUNTRY_OK){
        _get_country_code(country, country_json);
    }
    if (country->status == COUNTRY_OK){
        _get_country_capital(country, country_json);
    }
    if (country->status == COUNTRY_OK){
        _get_country_languages(country, country_json);
    }
    if (country->status == COUNTRY_OK){
        _get_country_borders(country, country_json);
    }
    if (country->status == COUNTRY_OK){
        _get_country_region(country, country_json);
    }
    return country;
}

/**
 * Free the memory allocated for a Country struct
 *
 * @param country   Country struct to free
 */
void _free_country(struct Country *country) {
    free(country->name);
    free(country->code);
    free(country->capital);
    free(country->languages);
    free(country->borders);
    free(country);
}

char * read_json_countries(char *json_abs_path) {
    char *json_countries;
    size_t file_size;
    FILE * f = fopen (json_abs_path, "r");

    if (f) {
        fseek (f, 0, SEEK_END);
        file_size = ftell (f);
        fseek (f, 0, SEEK_SET);
        json_countries = malloc ((file_size + 1) * sizeof(char));
        if (json_countries) {
            fread (json_countries, sizeof(char), file_size, f);
            json_countries[file_size] = '\0';
        }
        fclose (f);
    }
    else {
        fprintf(stderr, "error: %s could not be read.", json_abs_path);
    }
    return json_countries;
}

void free_json_countries(char *json_countries) {
    free(json_countries);
}

void print_text_countries(struct Country **countries, struct Arguments *args, FILE *f) {
    size_t i = 0;
    while ((struct Country*)countries[i] != NULL){
        fprintf(f, "Name: %s\n", countries[i]->name);
        fprintf(f, "Code: %s\n", countries[i]->code);
        if (args->show_capital) {
            fprintf(f, "Capital: %s\n", countries[i]->capital);
        }
        if (args->show_languages) {
            fprintf(f, "Languages: %s\n", countries[i]->languages);
        }
        if (args->show_borders) {
            fprintf(f, "Borders: %s\n", countries[i]->borders);
        }
        i++;
    }
}

void free_countries(struct Country **countries) {
    int i = 0;
    while (countries[i] != NULL) {
        _free_country(countries[i]);
        i++;
    }
}

int parse_json(struct Country ** countries, char *json_countries, struct Arguments *arguments) {
    json_t *root;
    json_error_t error;
    int country_count = 0;
    countries[0] = NULL;  /* empty list */

    root = json_loads(json_countries, 0, &error);
    if(!root) {
        fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
        return -1;
    } else if (!json_is_array(root)) {
        fprintf(stderr, "error: root is not an array\n");
        json_decref(root);
        return -1;
    }

    for (int i = 0; i < (int)json_array_size(root); i++) {
        struct Country *country;
        json_t *data;
        data = json_array_get(root, i);
        if (!json_is_object(data)) {
            fprintf(stderr, "error: country data %d is not an object\n", i + 1);
            json_decref(root);
            return -1;
        }
        country = _get_country(data, i);

        // Only add country to countries if it matches arguments criteria
        if (arguments->country) {
            if (_str_cmp(arguments->country, country->code) == 0) {
                countries[country_count] = country;
                country_count ++;
            } else {
                _free_country(country);
            }
        } else if (arguments->region) {
            if (_str_cmp(arguments->region, country->region) == 0) {
                countries[country_count] = country;
                country_count ++;
            } else {
                _free_country(country);
            }
        } else {
            countries[country_count] = country;
            country_count ++;
        }
    }
    countries[country_count] = NULL;  /* terminate list */

    json_decref(root);
    return country_count;
}
