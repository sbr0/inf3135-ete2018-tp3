/**
 * Responsible for handling graphic capabilities.
 *
 * @author Simon Brown
 */
#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdio.h>
#include "parse_args.h"
#include "parse_json.h"

#define PNG_FLAG_DIR "../countries/data"
#define MAX_CODE_LEN 5
#define MAX_CMD_LEN 50

/**
 * Output list or countries in dot format according to arguments
 *
 * @param countries   list of countries to print
 * @param args        arguments to consider for printing
 * @param f           file descriptor to print to
 */
void print_dot_countries(struct Country **countries, struct Arguments *args, FILE *f);

/**
 * Create png representation of countries using graphviz
 *
 * @param countries   list of countries to print
 * @param arguments   arguments to consider for printing
 * @return            0 on success, -1 overwise
 */
int gen_png_countries(struct Country **countries, struct Arguments *arguments);
#endif
